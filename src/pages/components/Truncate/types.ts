// third party
import { SemanticICONS } from "semantic-ui-react/dist/commonjs/generic"

export interface ITruncateType {
  content?: string,
  position?:
    'top left'
    | 'top right'
    | 'bottom right'
    | 'bottom left'
    | 'right center'
    | 'left center'
    | 'top center'
    | 'bottom center',
  icon?: SemanticICONS,
  length?: number,
  children?: React.ReactNode
}