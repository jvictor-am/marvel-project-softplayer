// third party
import {
  Icon,
  Popup,
} from 'semantic-ui-react'
import { truncate, size } from 'lodash'

// local
import { ITruncateType } from './types'


export const Truncate = ({ content, position = 'top left', icon, length = 25, children }: ITruncateType) => {
  if (size(content) > length) {
    if (children)
      return (
        <Popup
          position={ position }
          trigger={
            <div>
              { icon && <Icon name={ icon } /> }
              { truncate(content, {'length': length}) }
            </div>
          }>
          { children }
        </Popup>
      )

    return (
      <Popup
        content={ content }
        position={ position }
        trigger={
          <div>
            { icon && <Icon name={ icon } /> }
            { truncate(content, {'length': length}) }
          </div>
        } />
    )
  }

  return (
    <>
      { icon && <Icon name={ icon } /> }
      { content }
    </>
  )
}
export default Truncate