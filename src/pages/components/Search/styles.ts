// third party
import styled from 'styled-components'


export const Container = styled.div`
  .t-search-bar {
    border: 1px solid red;
    border-radius: 10em;
    padding: 0.6em 1em;
    margin-right: 1.5em;

    i {
      padding-left: 2.5em;
      color: red;
    }
  }

  .search-button {
    background: red !important;
    color: #FFFFFF !important;
    border: none !important;
    margin: 0.5em 0 !important;
    box-shadow: none !important;
    -webkit-box-shadow: none !important;

    :hover {
      opacity: 0.8;
    }
  }
`