export interface ISearchBar {
  placeholder: string;
  name?: string;
}