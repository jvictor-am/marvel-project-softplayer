// react
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'

// third party
import {
  Input,
  Button
} from 'semantic-ui-react'
import { isUndefined } from 'lodash'
import queryString from 'query-string'

// project
import { getLocationFilters } from '../utils/get-location-filters'

// local
import { Container } from './styles'
import { ISearchBar } from './types'


const SearchBar = ({ placeholder, name='nameStartsWith' }: ISearchBar) => {
  // state
  const [search, setSearch] = useState<string | string[] | null>('')

  // hooks
  const history = useHistory()
  const filters = getLocationFilters(history.location)

  useEffect(() => {
    if(!isUndefined(filters[name]))
    setSearch(filters[name])
  }, [])

  const handleInputSearch = (event: React.BaseSyntheticEvent) => {
    event.preventDefault()
    filters[name] = search
    history.push({ search: queryString.stringify(filters) })
  }

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if(event.key === 'Enter')
      handleInputSearch(event)
  }

  const onChangeSearch = (event: any, {value}: any) => setSearch(value)

  return (
    <Container>
      <Input
        icon='search'
        iconPosition='left'
        name='search'
        className='t-search-bar'
        onChange={onChangeSearch}
        value={search}
        placeholder={placeholder || ''}
        transparent
        onKeyPress={handleKeyPress}
      />
      <Button
        className='search-button'
        circular
        color='blue'
        type='submit'
        onClick={handleInputSearch}>
        Search
      </Button>
    </Container>
  )
}

export default SearchBar