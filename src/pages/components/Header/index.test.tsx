// third party
import { render, screen } from '@testing-library/react'

// local
import HeaderApp from './index'


it('Should render HeaderApp component correctly', () => {
  render(<HeaderApp title='testing'/>)
  const title = screen.getByText(/testing/i);
  expect(title).toBeInTheDocument();
})
