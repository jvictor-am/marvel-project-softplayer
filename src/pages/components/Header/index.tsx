// third party
import { useHistory } from 'react-router'
import { Popup } from 'semantic-ui-react'

// project
import MarvelLogo from '../../../assets/MarvelLogo.png'
import MarvelHeroes from '../../../assets/MarvelHeroes.png'

// local
import { HeaderAppProps } from './types'
import { HeaderContainer } from './styles'


const HeaderApp = ({ title }: HeaderAppProps) => {
  // hooks
  const history = useHistory()

  const goBack = () => history.push('/')

  return (
    <HeaderContainer>
      <Popup
        trigger={
          <img className='marvel-heroes-img' src={MarvelHeroes} alt='MarvelHeroes' onClick={goBack} />
        }
        content='Marvel Heroes'
        position='right center'
      />
      <h1>{title}</h1>
      <img src={MarvelLogo} alt='MarvelLogo' />
    </HeaderContainer>
  )
}

export default HeaderApp
