export interface ILocation {
  pathname?: string;
  search?: string;
  name?: string;
}