// third party
import queryString from 'query-string'
import {isEmpty, forEach, has} from 'lodash'

// local
import { ILocation } from './types'


export const getLocationFilters = (location: ILocation, filters?: string[]) => {
  const locationSearch = queryString.parse(location.search!)
  const locationFilters: any = {}

  if (isEmpty(filters))
    return locationSearch

  forEach(filters, (key) => {
    if (has(locationSearch, key))
      locationFilters[key] = locationSearch[key]
  })

  return locationFilters
}