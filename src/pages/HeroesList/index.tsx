// react
import { useEffect, useState } from 'react'

// third party
import { useHistory, useLocation } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import { Card, Dimmer, Icon, Image, Loader } from 'semantic-ui-react'

// project
import SearchBar from '../components/Search'
import HeaderApp from '../components/Header'
import Truncate from '../components/Truncate'
import { IState } from '../../store/modules/types'
import { fetchHeroes } from '../../store/modules/heroes/actions'
import { getLocationFilters } from '../components/utils/get-location-filters'
import { IHero, IHeroesListResponse } from '../../store/modules/heroes/types'

// local
import { ContainerCards } from './styles'
import { QUANTITY_OF_HEROES } from './constants'
import { IFilters } from './types'


const HeroesList = () => {
  //state
  const [loader, setLoader] = useState(true)

  // redux
  const heroesStore = useSelector<IState, IHeroesListResponse>(store => store.heroes)

  // hooks
  const dispatch = useDispatch()
  const history = useHistory()
  const location = useLocation()

  const filters = getLocationFilters(location, ['nameStartsWith', 'page'])

  useEffect(() => {
    const data: IFilters = {...filters}
    setLoader(true)
    dispatch(fetchHeroes(data, callbackLoader))
  }, [filters?.nameStartsWith, filters?.page])

  const callbackLoader = () => setLoader(false)

  const heroes = heroesStore?.results

  const goToSeriesPage = (heroId: number) => {
    history.push(`/series/${heroId}`)
  }

  return (
    <>
      <HeaderApp title='Marvel Heroes' />
      <div className='search-bar'>
        <SearchBar placeholder='Search by name...' />
      </div>
      <ContainerCards>
        <Card.Group className='group-cards'>
          {heroes?.map((hero: IHero) => (
            <Card className='card' key={hero.id}>
              <Image
                className='card-image'
                src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}
                alt={hero.name}
                wrapped
                ui={false} />
              <Card.Content>
                <Card.Header>
                  <Truncate content={hero.name} length={20} />
                </Card.Header>
              </Card.Content>
              <Card.Content extra>
                <a className='go-to-series' onClick={() => goToSeriesPage(hero.id)}>
                  <div>
                    Check comic series
                  </div>
                  <Icon name='sign-in alternate' size='large' />
                </a>
              </Card.Content>
            </Card>
          ))}
        </Card.Group>
        <Dimmer active={loader}>
          <Loader>Loading Heroes...</Loader>
        </Dimmer>
      </ContainerCards>
    </>
  )
}

export default HeroesList
