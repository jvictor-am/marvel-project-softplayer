// third party
import styled from 'styled-components'

export const ContainerCards = styled.div`
  margin: 2em 1.5em;

  @media screen and (max-width: 1920px), (min-width: 1920px) {
    /* 8 cards per row */
    .group-cards {
      grid-template-columns: repeat(8, 1fr);
    }
  }

  @media screen and (max-width: 1801px) {
    /* 6 cards per row */
    .group-cards {
      grid-template-columns: repeat(6, 1fr);
    }
  }

  @media screen and (max-width: 1367px) {
    /* 5 cards per row */
    .group-cards {
      grid-template-columns: repeat(5, 1fr);
    }
  }

  @media screen and (max-width: 680px) {
    /* 3 card per row */
    .group-cards{
      grid-template-columns: repeat(3, 1fr);
    }
  }

  .group-cards {
    display: inline-grid;
    column-gap: 0.5em;
    width: 100%;

    .card {
      width: auto;

      .go-to-series {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;

        :hover {
          color: red !important;
        }
      }
    }
  }
`
