// react
import { useEffect, useState } from 'react'

// third party
import { useHistory, useParams } from 'react-router'
import { Card, Dimmer, Loader, Image, Icon } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux'

// project
import HeaderApp from '../components/Header'
import Truncate from '../components/Truncate'
import { IState } from '../../store/modules/types'
import { fetchCurrentHeroSeries, fetchSeries } from '../../store/modules/series/actions'

// local
import { ISeriesParam } from './types'
import { ICurrentHeroResponse, ISerie, ISeriesListResponse } from '../../store/modules/series/types'
import { ContainerCards } from '../HeroesList/styles'


const Series = () => {
  //state
  const [loader, setLoader] = useState(true)

  // redux
  const seriesStore = useSelector<IState, ISeriesListResponse>(store => store.series)
  const currentHeroStore = useSelector<IState, ICurrentHeroResponse>(store => store.current_hero_series)

  // hooks
  const params: ISeriesParam = useParams()
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    dispatch(fetchSeries(params.heroId, callbackLoader))
    dispatch(fetchCurrentHeroSeries(params.heroId))
  }, [params.heroId])

  const callbackLoader = () => setLoader(false)

  const goBack = () => history.push('/')

  const series = seriesStore?.results
  const title = `Series "${currentHeroStore?.results[0]?.name}" has participated`


  return (
    <>
      <HeaderApp title={title} />
      <div className='go-back' onClick={goBack}>
        <Icon name='reply' color='red' size='big' />
        <h3>Go back</h3>
      </div>
      {
        series.length === 0 ? (
          <>
            <div className='no-series' >
              <Icon name='ban' color='red' size='huge' />
              <h1>No series registered for "{currentHeroStore?.results[0]?.name}"</h1>
            </div>
          </>
        ) : (
          <ContainerCards>
            <Card.Group className='group-cards'>
              {series?.map((serie: ISerie) => (
                <Card className='card' key={serie.id}>
                  <Image
                    className='card-image'
                    src={`${serie.thumbnail.path}.${serie.thumbnail.extension}`}
                    alt={serie.title}
                    wrapped
                    ui={false} />
                  <Card.Content>
                    <Card.Header>
                      <Truncate content={serie.title} length={30} />
                    </Card.Header>
                  </Card.Content>
                </Card>
              ))}
            </Card.Group>
          </ContainerCards>
        )
      }
      <Dimmer active={loader}>
        <Loader>Loading Series...</Loader>
      </Dimmer>
    </>
  )
}

export default Series
