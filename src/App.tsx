// react
import React from 'react'

// third party
import { Provider } from 'react-redux'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import 'semantic-ui-css/semantic.min.css'

// project
import GlobalStyle from './global_styles/global'
import store from './store'
import Routes from './routes'


const App = () => {
  return (
    <>
      <Provider store={ store }>
        <Routes />
        <ToastContainer position='bottom-right' autoClose={2000} theme='dark' />
      </Provider>
      <GlobalStyle />
    </>
  )
}

export default App
