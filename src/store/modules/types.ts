// local
import { IHeroesListResponse } from './heroes/types'
import { ISeriesListResponse, ICurrentHeroResponse } from './series/types'

export interface IState {
  heroes: IHeroesListResponse,
  series: ISeriesListResponse,
  current_hero_series: ICurrentHeroResponse
}