// local
import { ICurrentHeroResponse, ISeriesListResponse, SeriesTypes } from './types'


export function fetchSeries(heroId: string, callback?: () => void) {
  return {
    type: SeriesTypes.FETCH_SERIES,
    payload: heroId,
    callback
  }
}

export function fetchSeriesSuccess(response: ISeriesListResponse) {
  return {
    type: SeriesTypes.FETCH_SERIES_SUCCESS,
    payload: response
  }
}

export function fetchCurrentHeroSeries(heroId: string) {
  return {
    type: SeriesTypes.FETCH_CURRENT_HERO_SERIES,
    payload: heroId,
  }
}

export function fetchCurrentHeroSeriesSuccess(response: ICurrentHeroResponse) {
  return {
    type: SeriesTypes.FETCH_CURRENT_HERO_SERIES_SUCCESS,
    payload: response
  }
}
