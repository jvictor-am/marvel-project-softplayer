// local
import { fetchSeries, fetchCurrentHeroSeries } from './actions'


export enum SeriesTypes {
  FETCH_SERIES = 'FETCH_SERIES',
  FETCH_SERIES_SUCCESS = 'FETCH_SERIES_SUCCESS',
  FETCH_CURRENT_HERO_SERIES = 'FETCH_CURRENT_HERO_SERIES',
  FETCH_CURRENT_HERO_SERIES_SUCCESS = 'FETCH_CURRENT_HERO_SERIES_SUCCESS',
}

export interface ISerie {
  id: number;
  title: string;
  description: string;
  modified: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  resourceURI: string;
  urls: [
    {
      type: string;
      url: string;
    }
  ];
}

export interface ISeriesListResponse {
  count: number;
  limit: number;
  offset: number;
  total: number;
  results: ISerie[];
}

export interface ISeriesResponse {
  status: number;
  data: ISeriesListResponse;
}

export interface ICurrentHero {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: {
    path: string;
    extension: string;
  };
}

export interface ICurrentHeroResponse {
  count: number;
  limit: number;
  offset: number;
  total: number;
  results: ICurrentHero[];
}

export interface ICurrentHeroSeriesResponse {
  status: number;
  data: ICurrentHeroResponse;
}

export type fetchSeriesType = ReturnType<typeof fetchSeries>
export type fetchCurrentHeroSeriesType = ReturnType<typeof fetchCurrentHeroSeries>