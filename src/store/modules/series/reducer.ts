// third party
import { Reducer } from 'redux'

// local
import { ICurrentHeroResponse, ISeriesListResponse, SeriesTypes } from './types'


const STATE_DEFAULT: ISeriesListResponse & ICurrentHeroResponse = {
  count: 0,
  limit: 0,
  offset: 0,
  total: 0,
  results: [],
}

export const series: Reducer<ISeriesListResponse> = (state = STATE_DEFAULT, action) => {
  switch (action.type) {
    case SeriesTypes.FETCH_SERIES_SUCCESS:
      return action.payload

    default:
      return state
  }
}

export const current_hero_series: Reducer<ICurrentHeroResponse> = (state = STATE_DEFAULT, action) => {
  switch (action.type) {
    case SeriesTypes.FETCH_CURRENT_HERO_SERIES_SUCCESS:
      return action.payload

    default:
      return state
  }
}