// third party
import { AxiosResponse } from 'axios'
import { toast } from 'react-toastify'
import { all, call, put, takeLatest } from 'redux-saga/effects'

// project
import api from '../../../services/api'

// local
import {
  fetchCurrentHeroSeriesSuccess,
  fetchSeriesSuccess,
} from './actions'
import {
  SeriesTypes,
  fetchSeriesType,
  ISeriesResponse,
  fetchCurrentHeroSeriesType,
  ICurrentHeroSeriesResponse,
} from './types'


function* fetchSeriesSaga(action: fetchSeriesType) {
  try {
    const heroId = action.payload

    const response: AxiosResponse<ISeriesResponse> = yield call(api.get, `characters/${heroId}/series`)

    if (response.status === 200) {
      yield put(fetchSeriesSuccess(response.data.data))
      toast.success('Series loaded successfully !')
    }
  } catch (error) {
    toast.error('Something went wrong !')
  }

  action.callback?.()
}

function* fetchCurrentHeroSeriesSaga(action: fetchCurrentHeroSeriesType) {
  try {
    const heroId = action.payload

    const response: AxiosResponse<ICurrentHeroSeriesResponse> = yield call(api.get, `characters/${heroId}`)

    if (response.status === 200) {
      yield put(fetchCurrentHeroSeriesSuccess(response.data.data))
    }
  } catch (error) {
    toast.error('Something went wrong !')
  }
}

export default all([
  takeLatest(SeriesTypes.FETCH_SERIES, fetchSeriesSaga),
  takeLatest(SeriesTypes.FETCH_CURRENT_HERO_SERIES, fetchCurrentHeroSeriesSaga),
])