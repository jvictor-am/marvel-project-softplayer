// third party
import { all } from 'redux-saga/effects'

// local
import heroes_saga from './heroes/sagas'
import series_saga from './series/sagas'


export default function* rootSaga(): Generator {
  return yield all([
    heroes_saga,
    series_saga
  ])
}