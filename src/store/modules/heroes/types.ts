// local
import { fetchHeroes } from './actions'


export enum HeroTypes {
  FETCH_HEROES = 'FETCH_HEROES',
  FETCH_HEROES_SUCCESS = 'FETCH_HEROES_SUCCESS',
}

export interface IHeroDetail {
  available: number;
  collectionURI: string;
  items: [
    {
      resourceURI: string;
      name: string;
      type?: string;
    }
  ];
  returned: number;
}

export interface IHero {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: {
    path: string;
    extension: string;
  };
  resourceURI: string;
  comics: IHeroDetail;
  series: IHeroDetail;
  stories: IHeroDetail;
  events: IHeroDetail;
  urls: [
    {
      type: string;
      url: string;
    }
  ];
}

export interface IHeroesListResponse {
  count: number;
  limit: number;
  offset: number;
  total: number;
  results: IHero[];
}

export interface IHeroesResponse {
  status: number;
  data: IHeroesListResponse;
}

export type fetchHeroesType = ReturnType<typeof fetchHeroes>