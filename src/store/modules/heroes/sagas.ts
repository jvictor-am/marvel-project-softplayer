// third party
import { AxiosResponse } from 'axios'
import { toast } from 'react-toastify'
import { all, call, put, takeLatest } from 'redux-saga/effects'
import queryString from 'query-string'

// project
import api from '../../../services/api'
import { IFilters } from '../../../pages/HeroesList/types'

// local
import { fetchHeroesSuccess } from './actions'
import {
  HeroTypes,
  fetchHeroesType,
  IHeroesResponse,
} from './types'


function* fetchHeroesSaga(action: fetchHeroesType) {
  try {
    const filters: IFilters = action.payload!
    const search = queryString.stringify(filters)

    const response: AxiosResponse<IHeroesResponse> = yield call(api.get, `characters?${search}`)

    if (response.status === 200) {
      yield put(fetchHeroesSuccess(response.data.data))
      response.data.data.count === 0 ? (
        toast.warn('Try searching for another Hero !')
      ) : (
        toast.success('Heroes loaded successfully !')
      )
    }
  } catch (error) {
    toast.error('Something went wrong !')
  }

  action.callback?.()
}

export default all([
  takeLatest(HeroTypes.FETCH_HEROES, fetchHeroesSaga),
])