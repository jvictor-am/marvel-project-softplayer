// project
import { IFilters } from '../../../pages/HeroesList/types'

// local
import {
  HeroTypes,
  IHeroesListResponse,
} from './types'

export function fetchHeroes(filters?: IFilters, callback?: () => void) {
  return {
    type: HeroTypes.FETCH_HEROES,
    payload: filters,
    callback
  }
}

export function fetchHeroesSuccess(response: IHeroesListResponse) {
  return {
    type: HeroTypes.FETCH_HEROES_SUCCESS,
    payload: response
  }
}
