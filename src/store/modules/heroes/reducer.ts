// third party
import { Reducer } from 'redux'

// local
import {
  HeroTypes,
  IHeroesListResponse,
} from './types'


const STATE_DEFAULT: IHeroesListResponse = {
  count: 0,
  limit: 0,
  offset: 0,
  total: 0,
  results: [],
}

export const heroes: Reducer<IHeroesListResponse> = (state = STATE_DEFAULT, action) => {
  switch (action.type) {
    case HeroTypes.FETCH_HEROES_SUCCESS:
      return action.payload

    default:
      return state
  }
}