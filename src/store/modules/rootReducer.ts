// third party
import { combineReducers } from 'redux'

// local
import { heroes } from './heroes/reducer'
import { series, current_hero_series } from './series/reducer'


export default combineReducers({
  heroes,
  series,
  current_hero_series
})