// react
import React from 'react'

// third party
import { Route, BrowserRouter } from 'react-router-dom'

// project
import HeroesList from './pages/HeroesList'
import Series from './pages/Series'


const Routes = () => {
  return (
    <BrowserRouter>
      <Route component={HeroesList} path='/:page(\d+)?' exact />
      <Route component={Series} path='/series/:heroId(\d+)?' exact />
    </BrowserRouter>
  )
}

export default Routes
