// third party
import axios from 'axios'
import md5 from 'blueimp-md5'

// local
import { API_KEY, BASE_URL, PRIVATE_KEY } from './constants'


const timestamp = Date.now()
const api = axios.create({
  baseURL: BASE_URL
})

// When comunicating with Marvel API axios will delivery the configuration ...
// ... parameters (API_KEY, hash, timestamp) requested by the API
// API_KEY and PRIVATE_KEY come from .env file
api.interceptors.request.use((config) => {
  config.params = {
    ...config.params,
    ts: timestamp,
    apikey: API_KEY,
    hash: md5(timestamp + PRIVATE_KEY + API_KEY),
    limit: 50,
  }

  return config
})

export default api
