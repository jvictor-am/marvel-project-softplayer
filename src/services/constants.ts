export const PRIVATE_KEY = process.env.REACT_APP_PRIVATE_KEY as string
export const API_KEY = process.env.REACT_APP_API_KEY as string
export const BASE_URL: string = 'https://gateway.marvel.com:443/v1/public/'