<img alt="Marvel" title="Marvel Logo" src="src/assets/readme/0.MarvelReadme.png" width="200" />

<h2 align="center">
  Softplayer Marvel Project - JV
</h2>

## :computer: About

Project/ Challenge provided by Softplan to built a frontend application for consume [Marvel API](https://developer.marvel.com/docs). <br/>
[Check here](https://softplayermarveljv.netlify.app/) application deployed.

## Functionalities

- Hero List (limited to 50)
- Hero Search in all database
- Comics Series List for specific hero

## Built with

- React (^17.0.2)
- Yarn (1.22.5)
- Typescript (^4.1.2)
- ES6
- Styled Components (^5.3.1)
- Semantic-ui-react": (^2.0.3)
- React-Redux (^7.2.5)
- Redux-Saga (^1.1.3)
- React-router-dom (^5.3.0)
- React-toastify (^8.0.2)
- etc...

## :video_camera: Video

<h3 align="center">

<img src="src/assets/readme/VideoSoftplayerMarvelJV.mp4" width="700">

</h3>

## :camera: Application Screens

<h3 align="center">
  
<img src="src/assets/readme/1.LoadingHeroes.png" width="700"  alt="Home">

<details><summary><b>More Screens</b></summary>
  
<img src="src/assets/readme/1.LoadingHeroes.png" width="700"  alt="LoadingHeroes">
<img src="src/assets/readme/2.HeroesPage.png" width="700"  alt="HeroesPage">
<img src="src/assets/readme/3.SearchHeroPage.png" width="700"  alt="SearchHeroPage">
<img src="src/assets/readme/4.SeriesPage.png" width="700"  alt="SeriesPage">
<img src="src/assets/readme/5.NoSeriesPage.png" width="700"  alt="NoSeriesPage">

</details>
</h3>

## How to install and run:

<strong>1.</strong> Clone this project and install dependencies:

```
$ git clone https://gitlab.com/jvictor-am/marvel-project-softplayer.git
$ cd marvel-project-softplayer
$ yarn install
```

<strong>2.</strong> Get your developer [**KEYS**](https://developer.marvel.com/account) from Marvel Account and put them on *.env* file.

<strong>3.</strong> Run on localhost:

```
$ yarn start
```

---

# Author

[**João Victor**](https://www.linkedin.com/in/jo%C3%A3o-victor-de-andrade-mesquita-848a09122/)

<h2 align="center">
  Thank You!
</h2>
